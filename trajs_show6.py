from trajectory import *
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.patches as mpatches
#********************************************************************
# show the raw trajcetory
#********************************************************************

def read_origin(origin_file):
    with open(origin_file, "r") as f:
        origin_data = f.readlines()

    origins_traj = []

    origin = []
    for i in origin_data:
        start = i.find("[")
        end = i.find("]")
        points = i[start+1:end].split(" ")
        for j in points:
            if j != "":
                origin.append(j)

    x,y,z = 0,0,0
    for i in range(len(origin)):
        if i%3 == 0 :
            x = origin[i]
        elif i%3 == 1:
            y = origin[i]
        elif i%3 == 2:
            z = origin[i]
            origins_traj.append([float(x),float(y),float(z)])
    
    return origins_traj

def triangle():
    tri_width = 0.26
    tri_x = []
    tri_y = []
    for i in np.linspace(-tri_width/2,tri_width/2,100):
        if i <= 0:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(i+tri_width/2) - tri_width/2*np.tan(np.pi/6) )
        else:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(tri_width/2-i) - tri_width/2*np.tan(np.pi/6) )
    tri_x += tri_x
    tri_y += [-tri_width/2*np.tan(np.pi/6)]*100
    return [tri_x,tri_y]


def block_cf6():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices = [(-5, 2), (-1, 2), (-1, 1), (1, 1), (1,3.5), (5,3.5), (5,5), (-5,5), (0, 0)]
    # codes += [Path.MOVETO] + [Path.LINETO]
    # vertices += [(-1, -1.5), (1,-0.5)]
    codes += [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices += [(-5,-2), (-1,-2), (-1,-1), (1,-1), (1,-3.5), (5,-3.5), (5,-5), (-5,-5), (0, 0)]
    # codes += [Path.MOVETO] + [Path.LINETO]
    # vertices += [(-1, 0.5), (1, 1.5)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch

def crzayflie(cir_x=0.0,cir_y=0.0,color_in="#7CFC00",color_out="#4169E1",edgecolor_in = "black", edgecolor_out = "black"):
    def make_circle(r):
        t = np.arange(0, np.pi * 2.0, 0.01)
        t = t.reshape((len(t), 1))
        x = r * np.cos(t)
        y = r * np.sin(t)
        return np.hstack((x, y))

    vertices_inside = make_circle(0.06)
    vertices_outside = make_circle(0.10)
    codes = np.ones(
        len(vertices_inside), dtype=Path.code_type) * Path.LINETO
    codes[0] = Path.MOVETO
    
    # Shift the path
    vertices_inside[:, 0] += cir_x
    vertices_outside[:, 0] += cir_x
    vertices_inside[:, 1] += cir_y
    vertices_outside[:, 1] += cir_y

    # Create the Path object
    path_in = Path(vertices_inside, codes)
    path_out = Path(vertices_outside, codes)

    patch_in = PathPatch(path_in, facecolor=color_in, edgecolor=edgecolor_in)
    patch_out = PathPatch(path_out, facecolor=color_out, edgecolor=edgecolor_out)
    return [patch_out,patch_in]



def traj_show_map(traj_file,origin_file,block,middle_num=250,enable=[1,1,1]):

    raw_data = read_file(traj_file)
    origins_traj = read_origin(origin_file)

    multi_raw_trajs = multi_traj_process(raw_data)
    num_trajs = len(multi_raw_trajs)

    raw_traj = []
    for i in multi_raw_trajs:
        raw_traj.append(traj_multi(i))

    x = []
    y = []
    for i in range(len(raw_traj)):
        x.append( [j+origins_traj[i][0] for j in raw_traj[i].rawx] )
        y.append( [j+origins_traj[i][1] for j in raw_traj[i].rawy] )
    
    cf_start = []
    for i in range(len(raw_traj)):
        cf_start.append( crzayflie(origins_traj[i][0], origins_traj[i][1],color_table[i],color_table[i],"none") )

    cf_middle = []
    for i in range(len(raw_traj)):
        cf_middle.append( crzayflie( raw_traj[i].rawx[middle_num]+origins_traj[i][0], raw_traj[i].rawy[middle_num]+origins_traj[i][1],color_table[i],color_table[i],"none" ) )
    
    cf_end = []
    for i in range(len(raw_traj)):
        cf_end.append( crzayflie( raw_traj[i].rawx[-1]+origins_traj[i][0], raw_traj[i].rawy[-1]+origins_traj[i][1], color_table[i],color_table[i],"none" ) )

    fig,ax = plt.subplots()
    
    ax.add_patch(block)

    if enable[0]:
        for i in cf_start:
            ax.add_patch(i[0])
            ax.add_patch(i[1])
    if enable[1]:
        for i in cf_middle:
            ax.add_patch(i[0])
            ax.add_patch(i[1])
    if enable[2]:
        for i in cf_end:
            ax.add_patch(i[0])
            ax.add_patch(i[1])
    
    tri_xy = triangle()
    for i in range(len(raw_traj)):
        triangle_x = raw_traj[i].rawx[-1]+origins_traj[i][0] + tri_xy[0]
        triangle_y = raw_traj[i].rawy[-1]+origins_traj[i][1] + tri_xy[1]
        ax.plot(triangle_x,triangle_y,linestyle="-",color = color_table[i]+"99")

    an = np.linspace(0, 2 * np.pi, 100)
    for i in range(len(raw_traj)):
        circle_x = origins_traj[i][0] + 0.11*np.cos(an)
        circle_y = origins_traj[i][1] + 0.11*np.sin(an)
        ax.plot(circle_x,circle_y,linestyle="-",color = color_table[i]+"99")

    for i in range(num_trajs):
        ax.plot(x[i],y[i],linestyle=":",color = color_table[i]+"99")

    ax.set_xlabel("x [m]",size = 14)
    ax.set_ylabel("y [m]",size = 14)
    ax.set_xlim([-5,5])
    ax.set_ylim([-5,5])
    ax.tick_params(labelsize=16)
    ax.grid()
    ax.set_aspect(1.0)

    plt.tight_layout()
    plt.show()


color_table = [ "#4682B4", "#8B4513", "#00FF7F", "#FFA500", "#48D1CC", "#FF4500", "#B22222", "#808080", "#BDB76B", "#000080" ]

if __name__ == '__main__':
    #-----------------------------------------------------------------
    input_name = "./map_1008/10RobotsCorridorFormationChange_10s_500.txt"
    origin_file = "./trajectory_new/10RobotsCorridorFormationChange_10s_500/origin.txt"
    block = block_cf6()
    traj_show_map(input_name,origin_file,block,middle_num=100,enable=[0,1,0])
    block = block_cf6()
    traj_show_map(input_name,origin_file,block,middle_num=400,enable=[0,1,0])
from trajectory import *
import matplotlib.pyplot as plt 
from matplotlib.path import Path
from matplotlib.patches import PathPatch

#********************************************************************
# show the raw trajcetory
#********************************************************************

def read_origin(origin_file):
    with open(origin_file, "r") as f:
        origin_data = f.readlines()

    origins_traj = []

    origin = []
    for i in origin_data:
        start = i.find("[")
        end = i.find("]")
        points = i[start+1:end].split(" ")
        for j in points:
            if j != "":
                origin.append(j)

    x,y,z = 0,0,0
    for i in range(len(origin)):
        if i%3 == 0 :
            x = origin[i]
        elif i%3 == 1:
            y = origin[i]
        elif i%3 == 2:
            z = origin[i]
            origins_traj.append([float(x),float(y),float(z)])
    
    return origins_traj

def triangle():
    tri_width = 0.26
    tri_x = []
    tri_y = []
    for i in np.linspace(-tri_width/2,tri_width/2,100):
        if i <= 0:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(i+tri_width/2) - tri_width/2*np.tan(np.pi/6) )
        else:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(tri_width/2-i) - tri_width/2*np.tan(np.pi/6) )
    tri_x += tri_x
    tri_y += [-tri_width/2*np.tan(np.pi/6)]*100
    return [tri_x,tri_y]

def rectangle():
    tri_width = 0.26
    tri_x = []
    tri_y = []
    tri_x += np.linspace(-tri_width/2,tri_width/2,100).tolist()
    tri_y += [-tri_width/2]*100
    tri_x += [tri_width/2]*100
    tri_y += np.linspace(-tri_width/2,tri_width/2,100).tolist()
    tri_x += np.linspace(tri_width/2,-tri_width/2,100).tolist()
    tri_y += [tri_width/2]*100
    tri_x += [-tri_width/2]*100
    tri_y += np.linspace(-tri_width/2,tri_width/2,100).tolist()
    
    return [tri_x,tri_y]

def block_cf4():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices = [(-2, -1), (0.5, -1), (0.5, -2), (-2, -2), (0, 0)]
    # codes += [Path.MOVETO] + [Path.LINETO]
    # vertices += [(-1, -1.5), (1,-0.5)]
    codes += [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices += [(-0.5, 0.5), (-0.5, 2), (0.5, 2), (0.5,0.5), (0, 0)]
    # codes += [Path.MOVETO] + [Path.LINETO]
    # vertices += [(-1, 0.5), (1, 1.5)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch

def crzayflie(cir_x=0.0,cir_y=0.0,color_in="#7CFC00",color_out="#4169E1"):
    def make_circle(r):
        t = np.arange(0, np.pi * 2.0, 0.01)
        t = t.reshape((len(t), 1))
        x = r * np.cos(t)
        y = r * np.sin(t)
        return np.hstack((x, y))

    vertices_inside = make_circle(0.06)
    vertices_outside = make_circle(0.1)
    codes = np.ones(
        len(vertices_inside), dtype=Path.code_type) * Path.LINETO
    codes[0] = Path.MOVETO
    
    # Shift the path
    vertices_inside[:, 0] += cir_x
    vertices_outside[:, 0] += cir_x
    vertices_inside[:, 1] += cir_y
    vertices_outside[:, 1] += cir_y

    # Create the Path object
    path_in = Path(vertices_inside, codes)
    path_out = Path(vertices_outside, codes)

    patch_in = PathPatch(path_in, facecolor=color_in, edgecolor='black')
    patch_out = PathPatch(path_out, facecolor=color_out, edgecolor='black')

    return [patch_out,patch_in]

def traj_show_map(traj_file_1, traj_file_2, origin_file, block, point_num=350, middle_num = 350):

    raw_data_1 = read_file(traj_file_1)
    raw_data_2 = read_file(traj_file_2)
    origins_traj = read_origin(origin_file)

    multi_raw_trajs_1 = multi_traj_process(raw_data_1)
    num_trajs_1 = len(multi_raw_trajs_1)

    multi_raw_trajs_2 = multi_traj_process(raw_data_2)
    num_trajs_2 = len(multi_raw_trajs_2)

    raw_traj_1 = []
    for i in multi_raw_trajs_1:
        raw_traj_1.append(traj_multi(i))
    
    raw_traj_2 = []
    for i in multi_raw_trajs_2:
        raw_traj_2.append(traj_multi(i))

    x = []
    y = []
    for i in range(len(raw_traj_1)):
        x.append( [j+origins_traj[i][0] for j in raw_traj_1[i].rawx] )
        y.append( [j+origins_traj[i][1] for j in raw_traj_1[i].rawy] )
    x2 = []
    y2 = []
    for i in range(len(raw_traj_2)):
        x2.append( [j+origins_traj[i][0] for j in raw_traj_2[i].rawx[point_num:-1]] )
        y2.append( [j+origins_traj[i][1] for j in raw_traj_2[i].rawy[point_num:-1]] )    
    

    cf_middle = []
    for i in range(len(raw_traj_1)):
        cf_middle.append( crzayflie( raw_traj_1[i].rawx[middle_num]+origins_traj[i][0], raw_traj_1[i].rawy[middle_num]+origins_traj[i][1],color_table[i] ) )

    fig, ax = plt.subplots()

    ax.add_patch(block)
    for i in cf_middle:
        ax.add_patch(i[0])
        ax.add_patch(i[1])

    tri_xy = triangle()
    for i in range(len(raw_traj_1)):
        triangle_x = raw_traj_1[i].rawx[-1]+origins_traj[i][0] + tri_xy[0]
        triangle_y = raw_traj_1[i].rawy[-1]+origins_traj[i][1] + tri_xy[1]
        ax.plot(triangle_x,triangle_y,linestyle="--", color = color_table[i]+"CC")

    tri_xy = rectangle()
    for i in range(len(raw_traj_1)):
        triangle_x = raw_traj_2[i].rawx[-1]+origins_traj[i][0] + tri_xy[0]
        triangle_y = raw_traj_2[i].rawy[-1]+origins_traj[i][1] + tri_xy[1]
        ax.plot(triangle_x,triangle_y,linestyle="--",color = color_table[i]+"66")    

    an = np.linspace(0, 2 * np.pi, 100)
    for i in range(len(raw_traj_1)):
        circle_x = origins_traj[i][0] + 0.11*np.cos(an)
        circle_y = origins_traj[i][1] + 0.11*np.sin(an)
        ax.plot(circle_x,circle_y,linestyle="-.", color = color_table[i]+"CC")    

    for i in range(num_trajs_1):
        ax.plot(x[i],y[i],linestyle="--",color = color_table[i]+"CC")
    for i in range(num_trajs_2):
        ax.plot(x2[i],y2[i],linestyle=":",color = color_table[i]+"99")

    # ax.set_title("3D_Curve")
    ax.set_xlabel("x [m]",size = 14)
    ax.set_ylabel("y [m]",size = 14)

    ax.set_xlim([-2.5,2.5])
    ax.set_ylim([-2.0,2.0])

    ax.set_xticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_yticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.tick_params(labelsize=16)

    ax.grid()
    # ax.set_aspect(1.0)
    plt.tight_layout()
    plt.show()


color_table = [ "#4682B4", "#8B4513", "#00FF7F", "#FFA500", "#48D1CC", "#FF4500", "#B22222", "#808080", "#BDB76B", "#000080" ]

if __name__ == '__main__':

    input_name_1 = "./replanning/replanning_origin.txt"
    origin_file_1 = "./trajectory_new/replanning_origin/origin.txt"

    input_name_2 = "./replanning/replanning_replanned.txt"
    origin_file_2 = "./trajectory_new/replanning_replanned/origin.txt"

    block = block_cf4()
    traj_show_map(input_name_2,input_name_1,origin_file_1,block,point_num=350,middle_num=160)
    block = block_cf4()
    traj_show_map(input_name_2,input_name_1,origin_file_1,block,point_num=350,middle_num=350)

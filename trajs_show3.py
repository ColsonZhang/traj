"""
name : trajs_show3.py
purpose : draw the trajectory with the map
time : Oct 05. 2020
version : v1.00
author : colson_zhang
"""


from trajectory import *
import matplotlib.pyplot as plt 

#********************************************************************
# show the raw trajcetory
#********************************************************************

def read_origin(origin_file):
    with open(origin_file, "r") as f:
        origin_data = f.readlines()

    origins_traj = []

    origin = []
    for i in origin_data:
        start = i.find("[")
        end = i.find("]")
        points = i[start+1:end].split(" ")
        for j in points:
            if j != "":
                origin.append(j)

    x,y,z = 0,0,0
    for i in range(len(origin)):
        if i%3 == 0 :
            x = origin[i]
        elif i%3 == 1:
            y = origin[i]
        elif i%3 == 2:
            z = origin[i]
            origins_traj.append([float(x),float(y),float(z)])
    
    return origins_traj


def traj_show_map(traj_file,origin_file,map_config):

    offset_z = -1

    raw_data = read_file(traj_file)
    origins_traj = read_origin(origin_file)

    multi_raw_trajs = multi_traj_process(raw_data)
    num_trajs = len(multi_raw_trajs)


    raw_traj = []
    for i in multi_raw_trajs:
        raw_traj.append(traj_multi(i))

    x = []
    y = []
    z = []
    for i in range(len(raw_traj)):
        x.append( [j+origins_traj[i][0] for j in raw_traj[i].rawx] )
        y.append( [j+origins_traj[i][1] for j in raw_traj[i].rawy] )
        z.append( [j+origins_traj[i][2]+offset_z for j in raw_traj[i].rawz] )
    
    # new a figure and set it into 3d
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    x0 = map_config[0]
    y0 = map_config[1]
    bottom = map_config[2]
    width = map_config[3]
    depth = map_config[4]
    top = map_config[5]
    color = ["#0072E3"]*len(top)


    # draw the figure, the color is r = read
    for i in range(num_trajs):
        ax.plot(x[i], y[i], z[i])

    ax.bar3d(x0, y0, bottom, width, depth, top, color=color, shade=True)

    # set figure information
    #ax.set_title("3D_Curve")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.set_xlim([-2.5,2.5])
    ax.set_ylim([-2.0,2.0])
    ax.set_zlim([0.0,3.0])
    ax.set_xticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_yticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_zticks([0.0, 1.0, 2.0, 3.0])

    plt.show()

if __name__ == '__main__':

    # -------------------------------------
    # map configuration for the cf6
    # -------------------------------------
    # x0 = [-0.5,-0.5,-2.5,-2.5,0.5,0.5]
    # y0 = [0.75,-2.0,1.25,-2.0,-2.0,1.75]
    # bottom = [0.0,0.0,0.0,0.0,0.0,0.0]
    # width = [1.0,1.0,2.0,2.0,2.0,2.0]
    # depth = [1.25,1.25,0.75,0.75,0.25,0.25]
    # top = [0.8,0.8,0.8,0.8,0.8,0.8]

    # map_config = [x0,y0,bottom,width,depth,top]

    # --------------------------------------
    # draw the trajectory of the cf6
    # --------------------------------------
    # input_name = "./varying_corridor/6RobotsCorridorFormationChange_10s_500.txt"
    # origin_file = "./trajectory_new/6RobotsCorridorFormationChange_10s_500/origin.txt"
    # traj_show_map(input_name,origin_file,map_config)


    # -------------------------------------
    # map configuration for the cf4
    # -------------------------------------
    # x0 = [ -2.5,  -0.5]
    # y0 = [ -2.0, 0.5]
    # bottom = [ 0.0, 0.0]
    # width = [ 3.0, 1.0]
    # depth = [ 1.0, 1.5]
    # top = [ 0.8, 0.8]
    # map_config = [x0,y0,bottom,width,depth,top]
    

    # --------------------------------------
    # draw the trajectory of the cf4
    # --------------------------------------
    # input_name = "./replanning/replanning_origin.txt"
    # origin_file = "./trajectory_new/replanning_origin/origin.txt"
    # traj_show_map(input_name,origin_file,map_config)

    # input_name = "./replanning/replanning_replanned.txt"
    # origin_file = "./trajectory_new/replanning_replanned/origin.txt"
    # traj_show_map(input_name,origin_file,map_config)
    

    # -------------------------------------
    # map configuration for the cf2
    # -------------------------------------
    x0 = [ -1.0, -1.0 ]
    y0 = [ -1.5, 0.5 ]
    bottom = [ 0.0, 0.0 ]
    width = [ 2.0, 2.0 ]
    depth = [ 1.0, 1.0 ]
    top = [ 0.8, 0.8 ]

    map_config = [x0,y0,bottom,width,depth,top]

    # --------------------------------------
    # draw the trajectory of the cf2
    # --------------------------------------
    input_name = "./new_map/DualPointRobot2D_10s_500.txt"
    origin_file = "./trajectory_new/DualPointRobot2D_10s_500/origin.txt"
    traj_show_map(input_name,origin_file,map_config)
"""
==================================================================
name: trajectory.py
purpose: some class uesd to describe the trajectory
time: Sep 26. 2020
version: v1.0.0
author: Colson Zhang
==================================================================
"""

import matplotlib.pyplot as plt 
import numpy as np
import numpy.matlib
import cvxopt
import csv
import os

"""
=====================================================================
---------------------------THE CLASS PART----------------------------
=====================================================================
"""

#********************************************************************
# describe the data of one single point of the trajectory
#********************************************************************
class traj_sigle:
    def __init__(self,data):
        # when the raw data is not fixable, you can change the coefficient as you will.
        # even if the data's format is different, you can change the function.
        self.t = data[0]*2
        self.x = data[1]*0.9
        self.y = data[2]*0.9
        self.z = data[3]*1
        self.vx = data[4]*0.45
        self.vy = data[5]*0.45
        self.vz = data[6]*0.5

# describe the data of one total trajectory of one flight
class traj_multi:
    def __init__(self,data):
        # transform the raw data of a single line into the standard format
        self.traj = [traj_sigle(i) for i in data]
        
        # get the list of time, x ,y and z
        self.time = self.__get_element("t")
        self.origin_x = self.__get_element("x")
        self.origin_y = self.__get_element("y")
        self.origin_z = self.__get_element("z")
        self.vx = self.__get_element("vx")
        self.vy = self.__get_element("vy")
        self.vz = self.__get_element("vz")

        # get the number of the points of the trajcetory
        self.num = len(self.time)
        
        # get the origin point's x,y,z
        self.x0 = self.origin_x[0]
        self.y0 = self.origin_y[0]
        self.z0 = self.origin_z[0]
        
        # all the points of the trajectory subtract the origin point's (x,y,z)
        self.rawx = self.__process_raw_data("x")
        self.rawy = self.__process_raw_data("y")
        self.rawz = self.__process_raw_data("z")   # the value of z actually is a const, so there is no need of transforming z.
        #self.rawz = self.__process_rawz()


    # select the element you are interested in 
    def __get_element(self, element):
        term = []
        if element == "t":
            for i in self.traj:
                term.append( i.t )
        elif element == "x":
            for i in self.traj:
                term.append( i.x )
        elif element == "y":
            for i in self.traj:
                term.append( i.y )
        elif element == "z":
            for i in self.traj:
                term.append( i.z )
        elif element == "vx":
            for i in self.traj:
                term.append( i.vx )
        elif element == "vy":
            for i in self.traj:
                term.append( i.vy )
        elif element == "vz":
            for i in self.traj:
                term.append( i.vz )    
        return term
    
    # all the x(or y, z) subtract the origin point's x(or y, z)
    def __process_raw_data(self,element):
        raw_data = []
        if element == "x":
            origin_x_0 = self.origin_x[0]
            for i in self.origin_x:
                raw_data.append(i - origin_x_0)
        elif element == "y":
            origin_y_0 = self.origin_y[0]
            for i in self.origin_y:
                raw_data.append(i - origin_y_0)
        elif element == "z":
            origin_z_0 = self.origin_z[0]
            for i in self.origin_z:
                raw_data.append(i - origin_z_0)                  
        return raw_data

    # divide the element by the number of each list
    def __div_element(self,element,div_num):
        div_list = []
        for i in range(0, self.num, div_num):
            if i == 0:
                if   element == "t":
                    div_list.append(self.time[i:i+div_num])
                elif element == "x":
                    div_list.append(self.rawx[i:i+div_num])
                elif element == "y":
                    div_list.append(self.rawy[i:i+div_num])
                elif element == "z":
                    div_list.append(self.rawz[i:i+div_num])
                elif element == "vx":
                    div_list.append(self.vx[i:i+div_num])
                elif element == "vy":
                    div_list.append(self.vy[i:i+div_num])
                elif element == "vz":
                    div_list.append(self.vz[i:i+div_num])
            else:
                if   element == "t":
                    div_list.append(self.time[i-1:i+div_num])
                elif element == "x":
                    div_list.append(self.rawx[i-1:i+div_num])
                elif element == "y":
                    div_list.append(self.rawy[i-1:i+div_num])
                elif element == "z":
                    div_list.append(self.rawz[i-1:i+div_num])
                elif element == "vx":
                    div_list.append(self.vx[i-1:i+div_num])
                elif element == "vy":
                    div_list.append(self.vy[i-1:i+div_num])
                elif element == "vz":
                    div_list.append(self.vz[i-1:i+div_num])
        return div_list
        
    # pop the list whose number of elements is not enough
    def __div_select(self, raw_list, div_num):
        for i in range(len(raw_list)):
            if len(raw_list[i]) < div_num:
                raw_list.pop(i)
        return raw_list
    
    # get the divided time sequence.
    # ----------------------Attention!!!-------------------------------------#
    # The first element of each short time list is 0 s !!!
    # The last list's last element is the next list's first element (the lists are overlapped ).
    def div_t(self,div_num=10):
        term = self.__div_select(self.__div_element("t",div_num),div_num)
        time = []
        for i in term:
            time.append([j-i[0] for j in i])
        return time

    # get the divided x sequence.
    def div_x(self,div_num=10):
        return self.__div_select(self.__div_element("x",div_num),div_num)

    # get the divided y sequence.
    def div_y(self,div_num=10):
        return self.__div_select(self.__div_element("y",div_num),div_num)
    
    # get the divided z sequence.
    def div_z(self,div_num=10):
        return self.__div_select(self.__div_element("z",div_num),div_num)

    # get the divided z sequence.
    def div_vx(self,div_num=10):
        return self.__div_select(self.__div_element("vx",div_num),div_num)

    # get the divided z sequence.
    def div_vy(self,div_num=10):
        return self.__div_select(self.__div_element("vy",div_num),div_num)

    # get the divided z sequence.
    def div_vz(self,div_num=10):
        return self.__div_select(self.__div_element("vz",div_num),div_num)    
    


#********************************************************************
# describe the polynomial of the trajectory
#********************************************************************
class traj_poly:
    def __init__(self,duration,poly_x,poly_y,poly_z,poly_yaw=np.zeros(8)):
        self.duration = duration
        self.poly_x = poly_x
        self.poly_y = poly_y
        self.poly_z = poly_z
        self.poly_yaw = poly_yaw
        
    def get_poly(self):
        return [self.duration,self.poly_x,self.poly_y,self.poly_z,self.poly_yaw]




"""
=====================================================================
--------------------------THE FUNCTION PART--------------------------
=====================================================================
"""

#********************************************************************
# execute the calcualtion of polynomial fitting
#********************************************************************
def polynomial_match(x,y):
    # here I use the funcion of the module numpy
    # you can use other method to do the polynomial-fitting work
    polynomial = np.polyfit(x,y,7)
    return np.around(polynomial, decimals=6)

#********************************************************************
# fit the polynomial using cvxopt (quadprog)
#********************************************************************
def polynomial_cvxopt(t,pos,vel,degree=7):

    punishment = 0
    degree_standard = 7
    
    total_time = t[-1]
    
    punish_matrix = np.matlib.eye(degree+2)
    
    T = np.matrix(t).T
    
    Q = np.matlib.zeros((len(T),degree+1))
    Q[:, degree ] = 1
    for i in range(degree-1,-1,-1):
        Q[:,i]= np.multiply(T,Q[:,i+1])
    
    Real_Pos = np.matrix(pos).T
    Real_Vel = np.matrix(vel).T
    
    ExQ = np.append(-Real_Pos,Q,axis=1)
    H = 0.5*( ExQ.T*ExQ + punishment*punish_matrix )
    
    Aeq = np.matlib.zeros( (5, degree+2) )
    Aeq[0,:] = np.append(0,Q[0,:])
    Aeq[1,:] = np.append(0,Q[-1,:])
    Aeq[2, degree ] = 1
    for i in range(degree+1, 0, -1):
        Aeq[3,i] = (degree+1-i)*total_time**(degree-i)
    Aeq[4,0] = 1
    
    beq = [Real_Pos[0,0], Real_Pos[-1,0], Real_Vel[0,0],Real_Vel[-1,0],1]
    
    P = cvxopt.matrix(H.tolist())
    A = cvxopt.matrix(Aeq.T.tolist())
    b = cvxopt.matrix(beq)
    
    q = cvxopt.matrix([0.0 for i in range(degree+2)])
    G = cvxopt.matrix([0.0 for i in range(degree+2)],(1,degree+2))
    h = cvxopt.matrix([1.0])

    cvxopt.solvers.options['show_progress'] = False
    result = cvxopt.solvers.qp(P,q,G,h,A,b)
    res = list(result['x'])[1:]
    
    res2 = []
    for i in range(degree_standard-degree):
        res2.append(0)
    res = res2 + res
    
    return np.around(res, decimals=6)


#********************************************************************
# fit the polynomial using cvxopt (quadprog)
#********************************************************************
def polynomial_cvxopt_2(t,pos,vel,degree=7):

    punishment = 0
    degree_standard = 7
    
    total_time = t[-1]
    
    punish_matrix = np.matlib.eye(degree+2)
    
    T = np.matrix(t).T
    
    Q = np.matlib.zeros((len(T),degree+1))
    Q[:, degree ] = 1
    for i in range(degree-1,-1,-1):
        Q[:,i]= np.multiply(T,Q[:,i+1])
    
    Real_Pos = np.matrix(pos).T
    #Real_Vel = np.matrix(vel).T
    
    ExQ = np.append(-Real_Pos,Q,axis=1)
    H = 0.5*( ExQ.T*ExQ + punishment*punish_matrix )
    
    Aeq = np.matlib.zeros( (4, degree+2) )
    Aeq[0,:] = np.append(0,Q[0,:])
    Aeq[1,:] = np.append(0,Q[-1,:])
    Aeq[2, degree ] = 1
    #for i in range(degree+1, 0, -1):
    #    Aeq[3,i] = (degree+1-i)*total_time**(degree-i)
    Aeq[3,0] = 1
    
    beq = [Real_Pos[0,0], Real_Pos[-1,0], vel,1]
    
    P = cvxopt.matrix(H.tolist())
    A = cvxopt.matrix(Aeq.T.tolist())
    b = cvxopt.matrix(beq)
    
    q = cvxopt.matrix([0.0 for i in range(degree+2)])
    G = cvxopt.matrix([0.0 for i in range(degree+2)],(1,degree+2))
    h = cvxopt.matrix([1.0])

    cvxopt.solvers.options['show_progress'] = False
    result = cvxopt.solvers.qp(P,q,G,h,A,b)
    res = list(result['x'])[1:]
    
    res2 = []
    for i in range(degree_standard-degree):
        res2.append(0)
    res = res2 + res
    
    return np.around(res, decimals=6)

#********************************************************************
# execute the polynomial of several parts' x,y,z of a trajectory
#********************************************************************
def traj_generator(t,x,y,z):
    trajs = []
    for i in range(len(t)):
        duration = np.around(max(t[i]) - min(t[i]),6)
        poly_x = polynomial_match(t[i],x[i])
        poly_y = polynomial_match(t[i],y[i])
        poly_z = polynomial_match(t[i],z[i])
        
        # Flip the order of the coefficient of the polynomial
        # true format : [x^0, x^1, x^2,..., x^7]
        poly_x = np.flipud(poly_x)
        poly_y = np.flipud(poly_y)
        poly_z = np.flipud(poly_z)
        
        trajs.append(traj_poly(duration, poly_x, poly_y, poly_z))
    return trajs

#********************************************************************
# execute the polynomial of several parts' x,y,z of a trajectory
#********************************************************************
def traj_generator_2(t,x,y,z,vx,vy,vz):
    trajs = []
    for i in range(len(t)):
        duration = np.around(max(t[i]) - min(t[i]),6)
        poly_x = polynomial_cvxopt(t[i],x[i],vx[i], degree=7)
        poly_y = polynomial_cvxopt(t[i],y[i],vy[i], degree=7)
        poly_z = polynomial_cvxopt(t[i],z[i],vz[i], degree=7)
        
        # Flip the order of the coefficient of the polynomial
        # true format : [x^0, x^1, x^2,..., x^7]
        poly_x = np.flipud(poly_x)
        poly_y = np.flipud(poly_y)
        poly_z = np.flipud(poly_z)
        
        trajs.append(traj_poly(duration, poly_x, poly_y, poly_z))
    return trajs

#********************************************************************
# execute the polynomial of several parts' x,y,z of a trajectory
#********************************************************************
def traj_generator_3(t,x,y,z,vx,vy,vz):
    trajs = []
    
    temp_vx = vx[0][0]
    temp_vy = vy[0][0]
    temp_vz = vz[0][0]

    for i in range(len(t)):
        duration = np.around(max(t[i]) - min(t[i]),6)
        poly_x = polynomial_cvxopt_2(t[i],x[i],temp_vx, degree=7)
        poly_y = polynomial_cvxopt_2(t[i],y[i],temp_vy, degree=7)
        poly_z = polynomial_cvxopt_2(t[i],z[i],temp_vz, degree=7)
        
        # Flip the order of the coefficient of the polynomial
        # true format : [x^0, x^1, x^2,..., x^7]
        poly_x = np.flipud(poly_x)
        poly_y = np.flipud(poly_y)
        poly_z = np.flipud(poly_z)
        
        polynomial = traj_poly(duration, poly_x, poly_y, poly_z)
        
        temp_vx = test_traj(polynomial,duration)[3]
        temp_vy = test_traj(polynomial,duration)[4]
        temp_vz = test_traj(polynomial,duration)[5]
        
        trajs.append(polynomial)
    return trajs

#********************************************************************
# test the result by calculating the point using the polynomial
#********************************************************************
def test_traj(traj,t):
    [poly_duration,poly_x,poly_y,poly_z,poly_yaw] = traj.get_poly()
    
    res_x = 0
    res_y = 0
    res_z = 0
    for i in range(len(poly_x)):
        res_x = res_x*t + poly_x[len(poly_x)-1-i]
        res_y = res_y*t + poly_y[len(poly_y)-1-i]
        res_z = res_z*t + poly_z[len(poly_z)-1-i]
    
    res_vx = 0
    res_vy = 0
    res_vz = 0
    # pos = x0 + x1*t + x2*t^2 + ...... + x7*t^7
    # vel = x1 + 2*x2*t + 3*x3*t^2 + ......+ 7*x7*t^6
    for i in range(1,len(poly_x),1):
        # print(len(poly_x)-i)
        res_vx = res_vx*t + (len(poly_x)-i)*poly_x[len(poly_x)-i]
        res_vy = res_vy*t + (len(poly_y)-i)*poly_y[len(poly_y)-i]
        res_vz = res_vz*t + (len(poly_z)-i)*poly_z[len(poly_z)-i]
    
    res_ax = 0
    res_ay = 0
    res_az = 0
    # pos = x0 + x1*t + x2*t^2 + ...... + x7*t^7
    # vel = x1 + 2*x2*t + 3*x3*t^2 + ......+ 7*x7*t^6
    # acc = 1*2*x2 + 2*3*x3*t + 3*4*x4*t^2 + ...... + 6*7*x7*t^5
    for i in range(2,len(poly_x),1):
        res_ax = res_ax*t + (len(poly_x)-i)*(len(poly_x)-i+1)*poly_x[len(poly_x)+1-i]
        res_ay = res_ay*t + (len(poly_y)-i)*(len(poly_y)-i+1)*poly_y[len(poly_y)+1-i]
        res_az = res_az*t + (len(poly_z)-i)*(len(poly_z)-i+1)*poly_z[len(poly_z)+1-i]
        
    
    result = [res_x,res_y,res_z,res_vx,res_vy,res_vz,res_ax,res_ay,res_az]
    
    return np.around(result,decimals=6)

    

#********************************************************************
# process the data of the trajectory file of multi flights
#********************************************************************
def multi_traj_process(data):
    num_lines, num_rows = data.shape[0], data.shape[1]
    # number of the flight
    num_flight = int((num_rows-1)/6)
    #print(num_rows)
    # 7 : t, x, y, z, vx, vy, vz
    #multi_shape = (num_lines, 7)
    
    multi_data = []
    for j in range(num_flight):
        single_data = []
        for i in range(num_lines):
            single_data.append( [data[i][0], data[i][1+j*6], data[i][2+j*6], data[i][3+j*6], data[i][4+j*6], data[i][5+j*6], data[i][6+j*6]] )
        multi_data.append(np.array(single_data))
    
    return multi_data

#********************************************************************
# Get the t origin x y z vx vy vz
#********************************************************************
def traj_get_xyz(traj_data, divnum=40):
    raw_traj = traj_multi(traj_data)
    
    print("the origin (x,y,z) = ({:.3f},{:.3f},{:.3f})".format(raw_traj.x0,raw_traj.y0,raw_traj.z0))
    origin = [raw_traj.x0,raw_traj.y0,raw_traj.z0]
    origin = np.around(origin, decimals=4)

    t = raw_traj.div_t(div_num=divnum)
    x = raw_traj.div_x(div_num=divnum)
    y = raw_traj.div_y(div_num=divnum)
    z = raw_traj.div_z(div_num=divnum)
    vx = raw_traj.div_vx(div_num=divnum)
    vy = raw_traj.div_vy(div_num=divnum)
    vz = raw_traj.div_vz(div_num=divnum)

    return t,origin,x,y,z,vx,vy,vz


#********************************************************************
# transformer the trajectory from the points to the polynomials
#********************************************************************
def traj_transformer(traj_data, divnum = 40):

    t,origin,x,y,z,vx,vy,vz = traj_get_xyz(traj_data,divnum)

    #trajs = traj_generator(t,x,y,z)
    #trajs = traj_generator_2(t,x,y,z,vx,vy,vz)
    trajs = traj_generator_3(t,x,y,z,vx,vy,vz)
    
    output = [np.hstack((i.duration,i.poly_x,i.poly_y,i.poly_z,i.poly_yaw)) for i in trajs]
    
    return output,origin



#********************************************************************
# read file
#********************************************************************
def read_file(input_file):
    data = np.loadtxt(input_file)
    return data
    
#********************************************************************
# write file
#********************************************************************
def write_file(output_file, trajs_data):
    
    headers = ['duration','x^0','x^1','x^2','x^3','x^4','x^5','x^6','x^7',
               'y^0','y^1','y^2','y^3','y^4','y^5','y^6','y^7',
               'z^0','z^1','z^2','z^3','z^4','z^5','z^6','z^7',
               'yaw^0','yaw^1','yaw^2','yaw^3','yaw^4','yaw^5','yaw^6','yaw^7',]

    with open(output_file,'w')as f:
        f_csv = csv.writer(f)
        f_csv.writerow(headers)
        f_csv.writerows(trajs_data)
    
    print("the traj file is done!")


#********************************************************************
# transform the trajectory from the raw file to new file 
#********************************************************************
def top_trajfile_process(input_name, folder_name):
    raw_data = read_file(input_name)
    multi_raw_trajs = multi_traj_process(raw_data)

    if "trajectory_new" in os.listdir():
        pass
    else:
        os.mkdir("./trajectory_new/")

    if folder_name in os.listdir("./trajectory_new"):
        pass
    else:
        os.mkdir("./trajectory_new/"+folder_name)
    
    count = 1
    origins = []
    for single_traj in multi_raw_trajs:
        output_traj,origin = traj_transformer(single_traj)
        
        origins.append(origin)

        file_name ="./trajectory_new/"+folder_name+"/traj"+str(count)+'.csv'
        count += 1
        
        write_file(file_name, output_traj)
    
    file2_origin = "./trajectory_new/"+folder_name+"/origin.txt"
    origin_txt = ["traj"+str(i+1)+"\t"+str(origins[i])+"\n" for i in range(len(origins))]
    with open(file2_origin,'w')as f:
        f.writelines(origin_txt)   
    


#********************************************************************
# show the raw trajcetory
#********************************************************************
def traj_show(input_name):
    raw_data = read_file(input_name)
    multi_raw_trajs = multi_traj_process(raw_data)

    num_trajs = len(multi_raw_trajs)

    raw_traj = []
    for i in multi_raw_trajs:
        raw_traj.append(traj_multi(i))

    x = []
    y = []
    z = []
    for i in raw_traj:
        x.append(i.rawx)
        y.append(i.rawy)
        z.append(i.rawz)
    
    # new a figure and set it into 3d
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # set figure information
    ax.set_title("3D_Curve")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    # draw the figure, the color is r = read
    for i in range(num_trajs):
        ax.plot(x[i], y[i], z[i])

    plt.show()
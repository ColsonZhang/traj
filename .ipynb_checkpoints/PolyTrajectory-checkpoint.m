clc
clear all
%% initial param
punishment = 0;
degree = 7;
time_step = 0.01;
total_time = 1;
Real_Position = load("./trajectory/SinglePointRobot2D_500.txt");
T = [0:time_step:total_time]';
Q = zeros(size(T,1),degree+1);
punish_matrix = eye(degree+2);

%% get param of quadprog 
% definite for the problem
Q(:,degree+1) = 1;
for i=degree:-1:1
    Q(:,i) = T.* Q(:,i+1);
end
Real_Pose_x = Real_Position(:,2) - Real_Position(1,2);
Real_Pose_y = Real_Position(:,3) - Real_Position(1,3);
Real_Pose_z = Real_Position(:,4) - Real_Position(1,4);
Real_Vel_x = Real_Position(:,5) - Real_Position(1,5);
Real_Vel_y = Real_Position(:,6) - Real_Position(1,6);
Real_Vel_z = Real_Position(:,7) - Real_Position(1,7);
Real_x = Real_Pose_x(1:size(T,1));
ExQ = [-Real_x,Q];
H = 0.5 *( ExQ' * ExQ + punishment *punish_matrix);
% f= - Q' * Real_x;

% constraint condition 
Aeq = zeros(5,degree+2);
Aeq(1,:) = [0, Q(1,:)];
Aeq(2,:) = [0, Q(size(T,1),:)];
Aeq(3,degree+1) = 1;
for i=degree+2:-1:2
    Aeq(4,i) = (degree+2-i)*total_time^(degree+1-i);
end
Aeq(5,1) = 1;
%Aeq(3,:) = [0, 0, 0 ,0 , 1, 0]; 
%Aeq(4,:) = [5*total_time^4, 4*total_time^3, 3*total_time^2, 2*total_time^1, 1*total_time^0, 0];
beq = [Real_x(1), Real_x(size(T,1)), Real_Vel_x(1), Real_Vel_x(size(T,1)), 1]';

%% solve
poly_x = quadprog(H,[],[],[],Aeq,beq)
y = polyval(poly_x,T);
figure(1)
plot(T,y);
figure(2)
plot(T,Real_x);
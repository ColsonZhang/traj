"""
name : trajs_show7.py
purpose : plot the animation of thr trajectoies
version : v1.00
time : 00:52 . Oct 10. 2020
author : Colson Zhang
email : colson_z@outlook.com
"""

from trajectory import *
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.patches as mpatches

import itertools

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

#********************************************************************
# read the origin point
#********************************************************************
def read_origin(origin_file):
    with open(origin_file, "r") as f:
        origin_data = f.readlines()

    origins_traj = []

    origin = []
    for i in origin_data:
        start = i.find("[")
        end = i.find("]")
        points = i[start+1:end].split(" ")
        for j in points:
            if j != "":
                origin.append(j)

    x,y,z = 0,0,0
    for i in range(len(origin)):
        if i%3 == 0 :
            x = origin[i]
        elif i%3 == 1:
            y = origin[i]
        elif i%3 == 2:
            z = origin[i]
            origins_traj.append([float(x),float(y),float(z)])
    
    return origins_traj

#********************************************************************
# provide the plot of the triangle
#********************************************************************
def triangle():
    tri_width = 0.26
    tri_x = []
    tri_y = []
    for i in np.linspace(-tri_width/2,tri_width/2,100):
        if i <= 0:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(i+tri_width/2) - tri_width/2*np.tan(np.pi/6) )
        else:
            tri_x.append( i )
            tri_y.append( np.tan(np.pi/3)*(tri_width/2-i) - tri_width/2*np.tan(np.pi/6) )
    tri_x += tri_x
    tri_y += [-tri_width/2*np.tan(np.pi/6)]*100
    return [tri_x,tri_y]


#********************************************************************
# provide the block data of cf2
#********************************************************************
def block_cf2():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices = [(-1, -1.5), (1, -1.5), (1, -0.5), (-1, -0.5), (0, 0)]
    codes += [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices += [(-1, 0.5), (1, 0.5), (1, 1.5), (-1,1.5), (0, 0)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch

def block_cf4():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices = [(-2, -1), (0.5, -1), (0.5, -2), (-2, -2), (0, 0)]
    codes += [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
    vertices += [(-0.5, 0.5), (-0.5, 2), (0.5, 2), (0.5,0.5), (0, 0)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch

def block_cf6():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices = [(-2.5, 1.25), (-0.5, 1.25), (-0.5, 0.75), (0.5, 0.75), (0.5,1.75), (2.5,1.75), (2.5,2), (-2.5,2), (0, 0)]
    codes += [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices += [(-2.5, -1.25), (-0.5, -1.25), (-0.5, -0.75), (0.5, -0.75), (0.5, -1.75), (2.5, -1.75), (2.5,-2), (-2.5,-2), (0, 0)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch


def block_cf10():
    vertices = []
    codes = []
    codes = [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices = [(-5, 2), (-1, 2), (-1, 1), (1, 1), (1,3.5), (5,3.5), (5,5), (-5,5), (0, 0)]
    codes += [Path.MOVETO] + [Path.LINETO]*7 + [Path.CLOSEPOLY]
    vertices += [(-5,-2), (-1,-2), (-1,-1), (1,-1), (1,-3.5), (5,-3.5), (5,-5), (-5,-5), (0, 0)]

    path = Path(vertices, codes)
    pathpatch = PathPatch(path, facecolor='#778899', edgecolor='black')

    return pathpatch

def make_circle(r):
    t = np.arange(0, np.pi * 2.0, 0.01)
    t = t.reshape((len(t), 1))
    x = r * np.cos(t)
    y = r * np.sin(t)
    return np.hstack((x, y))

def get_cir_xy(raw_traj,origins_traj,id,num):
    cir_x = raw_traj[id].rawx[num] + origins_traj[id][0]
    cir_y = raw_traj[id].rawy[num] + origins_traj[id][1]        
    return [cir_x, cir_y]


def init():
    fig, ax = plt.subplots()

    ax.set_xlim([-2.5,2.5])
    ax.set_ylim([-2.0,2.0])
    ax.set_xticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_yticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_xlabel("x [m]",size = 14)
    ax.set_ylabel("y [m]",size = 14)
    ax.tick_params(labelsize=16)
    ax.grid()

    return fig,ax

def init_10():
    fig, ax = plt.subplots()

    ax.set_xlim([-5,5])
    ax.set_ylim([-5,5])
    # ax.set_xticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    # ax.set_yticks([-2.0, -1.0, 0.0, 1.0, 2.0])
    ax.set_xlabel("x [m]",size = 14)
    ax.set_ylabel("y [m]",size = 14)
    ax.tick_params(labelsize=16)
    ax.grid()

    return fig,ax    

def rectangle_points(min_x,min_y,max_x,max_y):
    tri_x = []
    tri_y = []

    tri_x += np.linspace( min_x, max_x, 100).tolist()
    tri_y += [min_y]*100
    tri_x += [max_x]*100
    tri_y += np.linspace( min_y, max_y ,100).tolist()
    tri_x += np.linspace( max_x, min_x, 100).tolist()
    tri_y += [max_y]*100
    tri_x += [min_x]*100
    tri_y += np.linspace( max_y, min_y, 100).tolist()  

    return [tri_x,tri_y]

def rect_line(trajs_x,trajs_y,middle_num,num_trajs):

    min_x = 100
    min_y = 100
    max_x = -100
    max_y = -100

    for i in range(num_trajs):
        if trajs_x[i][middle_num] < min_x:
            min_x = trajs_x[i][middle_num]
        if trajs_x[i][middle_num] > max_x:
            max_x = trajs_x[i][middle_num]

        if trajs_y[i][middle_num] < min_y:
            min_y = trajs_y[i][middle_num]
        if trajs_y[i][middle_num] > max_y:
            max_y = trajs_y[i][middle_num]            

    [tri_x,tri_y] = rectangle_points(min_x,min_y,max_x,max_y)

    return [tri_x,tri_y]

def area_line(middle_num):
    if middle_num <= 120:
        [rect_x, rect_y] = rectangle_points(-2.0, -0.8, -1.0, 0.8)
    elif (middle_num >180) and (middle_num <340):
        [rect_x, rect_y] = rectangle_points(-0.8, -0.5, 0.8, 0.5)
    elif (middle_num >380) and (middle_num <500):
        [rect_x, rect_y] = rectangle_points(1.4, -1.5, 2.0, 1.5)
    else:
        rect_x,rect_y = [],[]
    return [rect_x,rect_y]

def traj_animation(input_name,origin_file,block,fig,ax,save_option=0,save_name="test.gif"):

    # ***************************** 
    #        callback function
    # *****************************
    def run(data):
        middle_num = data

        for i in range(num_trajs):
            [cfs_cir_x[i], cfs_cir_y[i]] = get_cir_xy(raw_traj,origins_traj,id=i,num=middle_num)
            
        # Shift the path
        for i in range(num_trajs):
            vertice_cfs[i][:, 0] = vertices_standard[:, 0] + cfs_cir_x[i]
            vertice_cfs[i][:, 1] = vertices_standard[:, 1] + cfs_cir_y[i]

        # text_pt.set_text("t=%.2f s"%(data/50))
        
        # for i in range(num_trajs):
        #     lines[i].set_data( trajs_x[i][0:middle_num], trajs_y[i][0:middle_num] )
        
        # # rect line
        # [tri_x,tri_y] = rect_line(trajs_x,trajs_y,middle_num,num_trajs)
        # lines_rect.set_data(tri_x,tri_y)

        # area rect
        [rect_x, rect_y] = area_line(middle_num)
        area_rect.set_data( rect_x,rect_y )

        result = []
        # result = [text_pt]
        for i in range(num_trajs):
            # result.append(lines[i])
            result.append(patch_cfs[i])
        return result

    # ***************************** 
    #      initial data part
    # *****************************
    raw_data = read_file(input_name)
    origins_traj = read_origin(origin_file)

    multi_raw_trajs = multi_traj_process(raw_data)
    num_trajs = len(multi_raw_trajs)

    raw_traj = []
    for i in multi_raw_trajs:
        raw_traj.append(traj_multi(i))

    trajs_x = []
    trajs_y = []
    for i in range(len(raw_traj)):
        trajs_x.append( [j+origins_traj[i][0] for j in raw_traj[i].rawx] )
        trajs_y.append( [j+origins_traj[i][1] for j in raw_traj[i].rawy] )


    # ***************************** 
    #        static part
    # *****************************

    # -----add the block-----
    ax.add_patch(block)
    
    # # -----add the triangle-----
    # tri_xy = triangle()
    # for i in range(num_trajs):
    #     triangle_x = raw_traj[i].rawx[-1]+origins_traj[i][0] + tri_xy[0]
    #     triangle_y = raw_traj[i].rawy[-1]+origins_traj[i][1] + tri_xy[1]
    #     ax.plot(triangle_x,triangle_y,linestyle="--", color = color_table[i]+"B3")
    
    # # -----add the circle-----
    # an = np.linspace(0, 2 * np.pi, 100)
    # for i in range(num_trajs):
    #     circle_x = origins_traj[i][0] + 0.11*np.cos(an)
    #     circle_y = origins_traj[i][1] + 0.11*np.sin(an)
    #     ax.plot(circle_x,circle_y,linestyle="-.", color = color_table[i]+"B3") 

    # ***************************** 
    #        animation part
    # *****************************

    vertices_standard = make_circle(0.05)

    cfs_cir_x = []
    cfs_cir_y = []
    for i in range(num_trajs):
        [term_cir_x , term_cir_y] = get_cir_xy(raw_traj,origins_traj,id=i,num=0)
        cfs_cir_x.append(term_cir_x)
        cfs_cir_y.append(term_cir_y)

    vertice_cfs = []
    for i in range(num_trajs):
        vertice_cfs.append( vertices_standard + [cfs_cir_x[i], cfs_cir_y[i]] )

    codes = np.ones(
        len(vertices_standard), dtype=Path.code_type) * Path.LINETO
    codes[0] = Path.MOVETO

    # Create the Path object
    path_cfs = []
    for i in range(num_trajs):
        path_cfs.append( Path(vertice_cfs[i], codes) )

    patch_cfs = []
    for i in range(num_trajs):
        patch_cfs.append( PathPatch(path_cfs[i], facecolor=color_table[i], edgecolor='black') )

    for i in range(num_trajs):
        ax.add_patch(patch_cfs[i])

    # lines = []
    # for i in range(num_trajs):
    #     term_ax, = ax.plot([],[], linestyle="--", color = color_table[i]+"99")
    #     lines.append( term_ax )
    
    # lines_rect, = ax.plot([],[], linestyle= "-.", color = "#B22222"+"99")

    area_rect, = ax.plot([],[], linestyle= "-.", color = "#B22222"+"99")

    # text_pt = plt.text(text_x, text_y, '', fontsize=16)

    plt.tight_layout()

    ani = animation.FuncAnimation(fig, run, range(501) , interval=20)
    
    if save_option:
        if "animation" in os.listdir():
            pass
        else:
            os.mkdir("./animation/")
        
        if save_name[-4:] == ".mp4" :
            ani.save("./animation/"+save_name, fps=50, writer='ffmpeg')
        elif save_name[-4:] == ".gif" :
            ani.save("./animation/"+save_name, fps=50, writer='pillow')
    else:
        plt.show()



color_table = [ "#4682B4", "#8B4513", "#00FF7F", "#FFA500", "#48D1CC", "#FF4500", "#B22222", "#808080", "#BDB76B", "#000080" ]

if __name__ == '__main__':

    save_config = 0
    file_type = ".mp4" # ".mp4"

    # GENERAL SETTING
    text_x = 1.5
    text_y = 1.5

    # ----------------   cf6    ---------------------
    # input_name = "./varying_corridor/6RobotsCorridorFormationChange_10s_500.txt"
    # origin_file = "./trajectory_new/6RobotsCorridorFormationChange_10s_500/origin.txt"
    input_name = "./map_1010/map1010_6RobotsCorridorFormationChange_10s_500.txt"
    origin_file = "./trajectory_new/map1010_6RobotsCorridorFormationChange_10s_500/origin.txt"
    block = block_cf6()
    fig, ax = init()
    traj_animation(input_name,origin_file,block,fig,ax,save_option=save_config,save_name="cf6_special"+file_type)
    print("cf6 done")

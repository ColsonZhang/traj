from trajectory import *
import matplotlib.pyplot as plt 
import numpy as np
import csv
import os 


def processer(folder_name,input_name):
    print(input_name)
    # remember modify the file path
    file_name = "./"+folder_name+"/" + input_name + ".txt"
    top_trajfile_process(file_name, input_name)
    print()



if __name__ == '__main__':
    #processer("trajectory","4RobotsSquareFormation_500")
    #processer("trajectory","SinglePointRobot2D_500")
    processer("trajectory","DualPointRobot2D_500")

    #processer("new_map","6RobotsFormationChangePlanner_10s_500")
    #processer("new_map","4RobotsSquareFormation_500")
    processer("new_map","DualPointRobot2D_10s_500")
    # processer("new_map","DualPointRobot2D_10s_500")

    processer("varying_corridor","6RobotsCorridorFormationChange_10s_500")
    #processer("trajectory", "6RobotsCorridorFormationChange_10s_500_0.6m")

    processer("replanning","4RobotsSquareFormationReplanner_10s")
    #processer("replanning","4RobotsSquareFormationReplanner_10s_500")
    # processer("replanning","replanning_origin")
    # processer("replanning","replanning_replanned")

    # processer("map_1008","1008_4RobotsSquareFormationReplanner_10s_500")
    # processer("map_1008","1008DualPointRobot2D_10s_500")
    # processer("map_1008", "1008_4Robot_origin")
    # processer("map_1008", "1008_4Robot_replan")
    # processer("map_1008", "1008_4Robot_replan_2")
    # processer("map_1008","10RobotsCorridorFormationChange_10s_500")

    processer("map_1010","map1010_6RobotsCorridorFormationChange_10s_500")


'''
4RobotsSquareFormation_20s_500.txt         DualPointRobot2D_10s_500.txt
4RobotsSquareFormation_20s_50.txt          DualPointRobot2D_10s_50.txt
4RobotsSquareFormation_500.txt             DualPointRobot2D_500.txt
4RobotsSquareFormation_5s_500.txt          DualPointRobot2D.txt
4RobotsSquareFormation_5s.txt              SinglePointRobot2D_500.txt
4RobotsSquareFormation.txt                 SinglePointRobot2D_5s_500.txt
6RobotsFormationChangePlanner_10s_500.txt  SinglePointRobot2D_5s_50.txt
6RobotsFormationChangePlanner_10s_50.txt   SinglePointRobot2D.txt
'''
"""
6RobotsCorridorFormationChange_10s_500.txt
6RobotsCorridorFormationChange_10s_50.txt
6RobotsCorridorFormationChange_20s_500.txt
6RobotsCorridorFormationChange_20s_50.txt
"""
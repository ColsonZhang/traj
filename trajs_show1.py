from trajectory import *

#********************************************************************
# show the raw trajcetory
#********************************************************************

def read_origin(origin_file):
    with open(origin_file, "r") as f:
        origin_data = f.readlines()

    origins_traj = []

    origin = []
    for i in origin_data:
        start = i.find("[")
        end = i.find("]")
        points = i[start+1:end].split(" ")
        for j in points:
            if j != "":
                origin.append(j)

    x,y,z = 0,0,0
    for i in range(len(origin)):
        if i%3 == 0 :
            x = origin[i]
        elif i%3 == 1:
            y = origin[i]
        elif i%3 == 2:
            z = origin[i]
            origins_traj.append([float(x),float(y),float(z)])
    
    return origins_traj


def traj_show(traj_file,origin_file):
    raw_data = read_file(traj_file)
    origins_traj = read_origin(origin_file)

    multi_raw_trajs = multi_traj_process(raw_data)
    num_trajs = len(multi_raw_trajs)


    raw_traj = []
    for i in multi_raw_trajs:
        raw_traj.append(traj_multi(i))

    x = []
    y = []
    z = []
    for i in range(len(raw_traj)):
        # print("points",raw_traj[i].rawx[0],origins_traj[i][0])
        # print("num:{} x:{} y:{} z:{}".format(i,origins_traj[i][0],origins_traj[i][1],origins_traj[i][2]))
        x.append( [j+origins_traj[i][0] for j in raw_traj[i].rawx] )
        y.append( [j+origins_traj[i][1] for j in raw_traj[i].rawy] )
        z.append( [j+origins_traj[i][2] for j in raw_traj[i].rawz] )
    
    # new a figure and set it into 3d
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # set figure information
    ax.set_title("3D_Curve")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    # draw the figure, the color is r = read
    for i in range(num_trajs):
        ax.plot(x[i], y[i], z[i])

    plt.show()

if __name__ == '__main__':
    input_name = "./varying_corridor/6RobotsCorridorFormationChange_10s_500.txt"
    origin_file = "./trajectory_new/6RobotsCorridorFormationChange_10s_500/origin.txt"
    traj_show(input_name,origin_file)

    input_name = "./trajectory/4RobotsSquareFormation_500.txt"
    origin_file = "./trajectory_new/4RobotsSquareFormation_500/origin.txt"
    traj_show(input_name,origin_file)